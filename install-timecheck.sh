#!/bin/sh
# This script is released under the GPLv3 license
# See COPYING file for more information

isInstalled(){
	command -v "$@" >/dev/null 2>&1 || { echo "Programm $* is not installed but needed. Please install this programm first!"; exit 1; }
}

isWhiptailInstalled() {
	command -v whiptail > /dev/null 2>&1
	EXITCODE="$?"
	if [ "$EXITCODE" = 0 ]
		then
			USE_WHIPTAIL=true
		else
			USE_WHIPTAIL=false
	fi
}

createFiles(){
	if [ ! -f "$PRG_PATH""/time_MOFR.txt" ]
		then
			echo "-1" > "$PRG_PATH""/time_MOFR.txt"
	fi
	if [ ! -f "$PRG_PATH""/time_SASU.txt" ]
		then
			echo "-1" > "$PRG_PATH""/time_SASU.txt"
	fi
	if [ ! -f "$PRG_PATH""/week_MOFR.txt" ]
		then
			echo "-1" > "$PRG_PATH""/week_MOFR.txt"
	fi
	if [ ! -f "$PRG_PATH""/week_SASU.txt" ]
		then
			echo "-1" > "$PRG_PATH""/week_SASU.txt"
	fi
}

copyFiles() {
	cp timecheck.conf.example "$PRG_PATH/timecheck.conf" || { echo "An error occured during copying config file! Script execution aborted."; exit 1; }
	cp timecheck.sh "$PRG_PATH/timecheck.sh" || { echo "An error occured during copying main script! Script execution aborted."; exit 1; }
	cp COPYING "$PRG_PATH/COPYING" || { echo "An error occured during copying a file! Script execution aborted."; exit 1; }
	cp change_settings.sh "$PRG_PATH/change_settings.sh" || { echo "An error occured during copying a file! Script execution aborted."; exit 1; }
}

getUser(){
	if [ "$USE_WHIPTAIL" = "true" ]
		then
			USER=$(whiptail --inputbox "Which user should be controlled by Timecheck?" 8 60 --title "Timecheck konfiguration" 3>&1 1>&2 2>&3)
			EXITCODE="$?"
			if [ "$EXITCODE" = "0" ]
				then
					if [ -z "$USER" ]
						then
							echo "Invalid username! Script execution aborted."
							exit 1
					fi
				else
					echo "An error occured! Script execution aborted."
					exit 1
			fi
		else
			printf "Which user should be controlled by Timecheck? "
			read -r USER
	fi
}

getTime(){
	if [ "$USE_WHIPTAIL" = "true" ]
		then
			TIME_MOFR=$(whiptail --inputbox "How many minutes should $USER be allowed to use the account from monday to friday?" 9 60 --title "Timecheck configuration" 3>&1 1>&2 2>&3)
			EXITCODE="$?"
			if [ ! "$EXITCODE" = "0" ]
				then
					echo "Invalid input! Script execution aborted."
					exit 1
			fi
			TIME_SASU=$(whiptail --inputbox "How many minutes should $USER be allowed to use the account at the weekend?" 9 60 --title "Timecheck configuration" 3>&1 1>&2 2>&3)
			EXITCODE="$?"
			if [ ! "$EXITCODE" = "0" ]
				then
					echo "Invalid input! Script execution aborted."
					exit 1
			fi
		else
			printf "How many minutes should $USER be allowed to use the account from monday to friday? "
			read -r TIME_MOFR
			printf "How many minutes should $USER be allowed to use the account at the weekend? "
			read -r TIME_SASU
	fi
}

checkPermissions(){
	INSTALL_USER=$(whoami)
	if [ ! "$INSTALL_USER" = "root" ]
		then
			echo "You don't have enough permissions to perform this script. Please retry as root. Script execution aborted."
			exit 1
	fi
}


getPath(){
	if [ "$USE_WHIPTAIL" = "true" ]
		then
			PRG_PATH=$(whiptail --inputbox "Where should timecheck be installed? The directory will be created if it does not existing yet." 8 60 /opt/timecheck/ --title "Timecheck configuration" 3>&1 1>&2 2>&3)
			EXITCODE="$?"
			if [ ! "$EXITCODE" = "0" ]
				then
					echo "Invalid input! Script execution aborted."
					exit 1
			fi
		else
			printf "Where should timecheck be installed? The directory will be created if it does not existing yet. [/opt/timecheck/] "
			read -r PRG_PATH
			if [ -z "$PRG_PATH" ]
				then
					PRG_PATH=/opt/timecheck/
			fi
	fi
	case $PRG_PATH in
		*/)
		;;
		*) PRG_PATH=$PRG_PATH/
		;;
	esac

}

getCleanUp(){
	if [ "$USE_WHIPTAIL" = "true" ]
		then
			whiptail --yesno "Should timecheck remove all files that have been downloaded with 'git clone' but are no longer needed?" 9 60 --defaultno --title "Timecheck configuration" 3>&1 1>&2 2>&3
			EXITCODE="$?"
			if [ "$EXITCODE" = "0" ]
				then
					CLEAN_UP=true
				else
					CLEAN_UP=false
			fi
		else
			printf "Should timeckeck remove all files that have been downloaded with 'git clone' but are no longer needed? [y/N] "
			read -r CLEAN_UP_REQUEST
			if [ -z "$CLEAN_UP_REQUEST" ]
				then
					CLEAN_UP=false
				else
					case $CLEAN_UP_REQUEST in
						y|Y) CLEAN_UP = true
						;;
						n|N) CLEAN_UP = false
						;;
						*) echo "This was not a valid input. Script execution aborted." && exit 1
						;;
					esac
			fi
	fi
}

allCorrect(){
	if [ "$USE_WHIPTAIL" = "true" ]
		then
			whiptail --yesno "Are these information correct? \n \n Timecheck will be installed to: $PRG_PATH \n User to be controlled: $USER \n Time from monday to friday: $TIME_MOFR \n Time from Saturday to Sunday: $TIME_SASU \n Timecheck should clean up at the end: $CLEAN_UP" 13 60 --defaultno --title "Timecheck configuration" 3>&1 1>&2 2>&3
			EXITCODE="$?"
			if [ ! "$EXITCODE" = "0" ]
				then
					echo "Please correct the information by running this script again! Script execution aborted."
					exit 1
			fi
		else
			echo "We have collected all needed information. Please check if these informaiton are correct."
			echo "Timecheck will be installed to 	$PRG_PATH"
			echo "User to be controlled:		$USER"
			echo "Time from monday to friday:	$TIME_MOFR"
			echo "Time at the weekend:		$TIME_SASU"
			echo "Timecheck will clean up:	$CLEAN_UP"
			echo ""
			printf "Are these information correct? [y|n] "
			read -r INFORMATION_CORRECT
			case $INFORMATION_CORRECT in
				y|Y)
				;;
				n|N) echo "Please correct the information by running this script again! Script execution aborted." && exit 1
				;;
			esac
	fi
}
createDir(){
	if [ ! -d "$PRG_PATH" ]
		then
			mkdir -p "$PRG_PATH"
	fi
}

setFilePermission(){
	chown root:root -R "$PRG_PATH"
	EXITCODE="$?"
	if [ ! "$EXITCODE" = "0" ]
		then
			echo "An error occured! Script execution aborted."
			exit 1
	fi

	chmod 700 -R "$PRG_PATH"
	EXITCODE="$?"
	if [ ! "$EXITCODE" = "0" ]
		then
			echo "An error occured! Script execution aborted."
			exit 1
	fi
	chown root:root "$PRG_PATH/timecheck.sh"
	chmod 744 "$PRG_PATH/timecheck.sh"
}

exportVariable(){
	sed -i 's/USER=YourUserHere/USER='"$USER"'/' "$PRG_PATH""timecheck.conf"
	sed -i 's/TIME_MOFR=100/TIME_MOFR='"$TIME_MOFR"'/' "$PRG_PATH""timecheck.conf"
	sed -i 's/TIME_SASU=100/TIME_SASU='"$TIME_SASU"'/' "$PRG_PATH""timecheck.conf"
	# it is needed here to use | instead of / for sed because of the path in $PRG_PATH
	sed -i 's|PRG_PATH=/opt/timecheck|PRG_PATH='"$PRG_PATH"'|' "$PRG_PATH""timecheck.conf"
}

exportCronjob(){
	echo ""
	echo "We are allmost done. Just one thing is missing"
	echo "Please add the following to the end of the root crontab"
	echo "open the crontab with 'sudo crontab -e'"
	echo ""
	echo "@reboot $PRG_PATH""timecheck.sh $PRG_PATH"
	echo "* * * * * $PRG_PATH""timecheck.sh $PRG_PATH"
	echo ""

}

exportInformation(){
	echo ""
	echo "Timecheck is installed to $PRG_PATH"
	echo "if you want to change the configuration please run"
	echo "change-settings.sh in $PRG_PATH"
	echo ""
	echo "You may also edit timecheck.conf by your own"
	echo "but this is not recommended!"
	echo ""
}

cleanUp(){
	LCL_PATH=$(pwd -P)
        case $LCL_PATH in
                */)
                ;;
                *) LCL_PATH=$LCL_PATH/
                ;;
        esac
	if [ "$CLEAN_UP" = "true" ] && [ ! "$PRG_PATH" = "$LCL_PATH" ]
		then
			printf "Are you sure you want me to clean up here? Then type 'yEs' "
			read -r CLEAN_UP
			if [ "$CLEAN_UP" = "yEs" ]
				then
					echo "OK. I'm going to clean up a little..."
					rm -r .git .gitignore COPYING README.md README_de.md timecheck.conf.example timecheck.sh
					echo "Self-destruction initiated"
					rm -- "$0"
					echo "done"
				else
					echo "OK. Clean up yourself."
					exit 1
			fi
	elif [ "$PRG_PATH" = "$LCL_PATH" ]
		then
			echo "Error! Files in this directory are still needed!"
			echo "I do not delete any files"
	fi
}

main(){
	isWhiptailInstalled
	checkPermissions
	for PRG in sudo notify-send
	do
		isInstalled $PRG
	done
	getPath
	getUser
	getTime
	getCleanUp
	allCorrect
	createDir
	copyFiles
	createFiles
	setFilePermission
	exportVariable
	exportCronjob
	exportInformation
	cleanUp
}
main

